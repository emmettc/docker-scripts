#!/bin/bash
if [ "$1" = '-?' ];then
  echo "$(basename $0) - List docker images"
  exit
fi  
docker images
